
"use strict";

Function.prototype.myBind = function(context)
{
  var fn = this;
  return function() {
    return fn.apply(context);
  }
};


function Baratheon(name, childNum) {
  this.name = name;
  this.childNum = childNum;
}

Baratheon.prototype.sayName = function() {
  return ("I am " + this.name + " and I have " + this.childNum + " children, bastard or otherwise");
};

var stannis = new Baratheon("Stannis", 1);

var sayNameUnbound = stannis.sayName;
var sayNameBound = stannis.sayName.myBind(stannis);

// sayNameUnbound();
sayNameBound();
