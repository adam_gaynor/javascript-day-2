"use strict";
var readline = require('readline');
var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function HanoiGame(stackSize) {
  this.leftStack = [];
  this.centerStack = [];
  this.rightStack = [];

  for (var i = 0; i < stackSize; i++) {
    this.leftStack.push(i+1);
  }
}

HanoiGame.prototype.run = function(completionCallback) {
  console.log("Welcome to towers of Hanoi");
  if (!this.isWon()) {
    this.print();
    this.promptMove(function (start, end) {
      this.move(start, end);
      this.run(completionCallback);
    }.bind(this));
  } else {
    completionCallback();
  }
}

HanoiGame.prototype.isWon = function() {
  if (this.leftStack.length === 0) {
    if (this.centerStack.length === 0 || this.rightStack.length === 0) {
      return true;
    }
  }
  return false;
}

HanoiGame.prototype.promptMove = function(callback) {
  var self = this;
  var input = reader.question("What is your move? (ex. Left to Center)",
    function(userInput) {
    userInput = userInput.split(" to ");
    var start = self.getStack(userInput[0]);
    var end = self.getStack(userInput[1]);
    callback(start, end);
  })
}

HanoiGame.prototype.move = function(startTower, endTower) {
  var self = this;
  if (this.isValidMove(startTower, endTower)) {
    endTower.unshift(startTower.shift());
  } else {
    console.log("Invalid move");
  }
  // this.run(this.endGame.bind(this));
}


HanoiGame.prototype.isValidMove = function(startTower, endTower) {
  if (startTower.length === 0) {
    return false;
  }
  if (endTower.length === 0 || endTower[0] > startTower[0]) {
    return true;
  }
  return false;
}

HanoiGame.prototype.getStack = function(stackName) {
  switch (stackName.toLowerCase()) {
  case "left":
    return this.leftStack;
    break;
  case "right":
    return this.rightStack;
    break;
  case "center":
    return this.centerStack;
    break;
  default:
    console.log("Invalid stack name " + stackName);
  }
}

HanoiGame.prototype.print = function() {
  console.log("Left Stack: " + this.leftStack);
  console.log("Right Stack: " + this.rightStack);
  console.log("Center Stack: " + this.centerStack);
}

HanoiGame.prototype.endGame = function () {
  this.print();
  console.log("Game over");
  reader.close();
};


var hanoi = new HanoiGame(1);
hanoi.run(hanoi.endGame.bind(hanoi));
