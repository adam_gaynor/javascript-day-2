"use strict";

function Board(game) {
  this.grid = [[null, null, null],[null, null, null],[null, null, null]];
  this.game = game;
}

Board.prototype.print = function() {
  console.log(this.grid[0]);
  console.log(this.grid[1]);
  console.log(this.grid[2]);
}

Board.prototype.placeMove = function(x, y, mark) {
  if (this.validMove(x,y)) {
    this.grid[y][x] = mark;
  } else {
    console.log("invalid coordinates");
  }
  this.game.run(mark);
}

Board.prototype.validMove = function(x,y) {
  if (x > 2 || x < 0 || y > 2 || y < 0) {
    return false;
  } else if (this.grid[y][x] != null ) {
    return false;
  } else {
    return true;
  }
}
Board.prototype.isWon = function() {
  var transposedArray = myTranspose(this.grid);

  for (var i = 0; i <= 2; i++) {
    if (this.grid[i][0] != null &&
        this.grid[i][0] === this.grid[i][1] &&
        this.grid[i][1] === this.grid[i][2]) {
          return true;
        }
    if (transposedArray[i][0] != null &&
        transposedArray[i][0] === transposedArray[i][1] &&
        transposedArray[i][1] === transposedArray[i][2]) {
          return true;
        }
    }
  if (this.grid[0][0] != null &&
      this.grid[0][0] == this.grid[1][1] &&
      this.grid[2][2] == this.grid[1][1]) {
        return true;
      }
  if (this.grid[0][2] != null &&
      this.grid[0][2] == this.grid[1][1] &&
      this.grid[2][0] == this.grid[1][1]) {
        return true;
      }

      return false;
}

function myTranspose(array) {
  var transposedArray = [];

  for(var i = 0; i < array[0].length; i++){
    transposedArray[transposedArray.length] = [];
  }

  for(var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      transposedArray[j][i] = array[i][j];
    }
  }

  return transposedArray;
};

module.exports = Board;
