"use strict";
var Board = require("./board");

function Game(reader) {
  this.reader = reader;
  this.board = new Board(this);
};

Game.prototype.run = function (mark) {
  mark = mark === "x" ? "o" : "x";
  console.log("Welcome to tic-tac-toe");
  this.board.print();
  if (!this.board.isWon()) {
    //this.promptMove(this.board.placeMove.bind(this.board), mark);
    this.promptMove((function(x, y) {
      this.board.placeMove(x, y, mark);
      this.run(mark);
    }).bind(this), mark);
  } else {
    console.log("Game over, " + mark + " wins.");
    this.reader.close();
  }
};


Game.prototype.promptMove = function(callback, mark) {
  this.reader.question(mark + ", choose a position ex.(1,2): ",
        function(userInput){
          userInput = userInput.split(",");
          var row = userInput[0];
          var col = userInput[1];
          callback(row, col, mark);
        });
};

module.exports = Game;
