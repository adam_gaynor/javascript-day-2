// Ways to Declare a Function
1) named function
function func() {

};

2) as part of an object
var obj = {
  sayHi: function () {

  }
};

3) anonymous function to a variable
var x = function () {

};

4) anonymous function
[].forEach(function () {

});

5) IIFE - used to create scope (Immediately invoked function expression)
(function () {
  var x = 1;


})();

x // not defined



// 3 ways to call a function
1) method style
cat.sayHi();
this === object it was called on / the receiver

2) function style
helloWorld();
this === global object

3) constructor style
new Cat();
this === new object
// return value thrown out, returns new object

===========================================
// jk there are 5
4) apply
5) call

var cat = {
  name: "Sennacy",
  sayHi: function () {
    return "hi my name is " + this.name;
  },
  addTwoNums: function (a, b) {
    return "hi my name is " + this.name + " and the sum is " + (a + b);
  }
};

cat.sayHi();
cat.sayHi.call(cat);

var sayHiFunc = cat.sayHi;
sayHiFunc(); // "hi my name is "
sayHiFunc.call(cat); // "hi my name is Sennacy"
sayHiFunc.apply(cat); // "hi my name is Sennacy"

var dog = {
  name: "Nigel Ruffler"
};

sayHiFunc.call(dog); // "hi my name is Nigel Ruffler"
sayHiFunc.call({ name: "Firetrux" }); // "hi my name is Firetrux"

var helloWorld = function () {
  console.log(this.superpower);
};

helloWorld(); // undefined
helloWorld.call({ superpower: "x-ray vision" }); // x-ray vision

var cat = {
  name: "Sennacy",
  sayHi: function () {
    return "hi my name is " + this.name;
  },
  addTwoNums: function (a, b) {
    return "hi my name is " + this.name + " and the sum is " + (a + b);
  }
};

cat.addTwoNums(1, 1);
cat.addTwoNums.call(dog, 1, 2);
cat.addTwoNums.apply(dog, [1, 2]);

=======================================================
bind // DOES NOT INVOKE FUNCTION - returns a NEW FUNCTION with `this` set

var catSayHi = cat.sayHi.bind(cat);
catSayHi(); // "hi my name is Sennacy"

// poor man's version
var poorCatSayHi = function () {
  cat.sayHi();
};

poorCatSayHi(); // "hi my name is Sennacy"



var cat = {
  name: "Sennacy",
  sayHi: function () {
    return "hi my name is " + this.name;
  },
  addTwoNums: function (a, b) {
    return "hi my name is " + this.name + " and the sum is " + (a + b);
  },
  countToThree: function () {
    var arr = [1, 2, 3];

    arr.forEach(function (el) {
      console.log("hi my name is " + this.name + " and the el is " + el);
    });



    // 1. that = this
    var cat = this;

    arr.forEach(function (el) {
      console.log("hi my name is " + cat.name + " and the el is " + el);
    });

    // 2. bind
    arr.forEach(function (el) {
      this // global object, until bound to something else
      console.log("hi my name is " + this.name + " and the el is " + el);
    }.bind(this));

    this // cat object (most likely)

    // 3.
    var myCountFunc = function (el) {
      console.log("hi my name is " + this.name + " and the el is " + el);
    };

    arr.forEach(myCountFunc.bind(this));
  }
};



var times = function (num, callback) {
  for (var i = 0; i < num; i++) {
    callback(); // function style
  }
};

times(3, cat.sayHi); // "hi my name is " x3

var catSayHi = cat.sayHi;
times(3, catSayHi); // "hi my name is " x3

times(3, cat.sayHi.bind(cat)); // "hi my name is Sennacy" x3
// DO THIS ^

times(3, cat.sayHi()); // string is not a function
// NEVER DO THIS ^
times(3, cat.sayHi.call(cat));
// same as above ^

times(3, function () {
  cat.sayHi();
}); // "hi my name is Sennacy" x3
// eh ^


// THE FIRST BIND TAKES ALL PRECEDENCE
// (the first cut is the deepest)
var boundFunc = function () {
  cat.sayHi();
};

boundFunc.bind(dog);
