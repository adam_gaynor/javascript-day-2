"use strict";
var readline = require('readline');
var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function addNumbers(sum, numsLeft, completionCallback) {
  if (numsLeft > 0) {
    //prompt for input
    var userInput = reader.question("Enter Number: ", function (numString1) {
      var num1 = parseInt(numString1);
      addNumbers(sum + num1, numsLeft - 1, completionCallback);
    })
  }
  //base case
  if (numsLeft === 0) {
    completionCallback(sum);
  }
};

addNumbers(0, 4, function (totalSum) {
  console.log("The sum is: " + totalSum);
  reader.close();
});
